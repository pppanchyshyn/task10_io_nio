package com.epam.task10.io.nio.model.messanger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client1 {

  private final static int ServerPort = 1111;

  public static void main(String args[]) throws IOException {
    Scanner input = new Scanner(System.in);
    InetAddress ip = InetAddress.getByName("localhost");
    Socket s = new Socket(ip, ServerPort);
    DataInputStream dis = new DataInputStream(s.getInputStream());
    DataOutputStream dos = new DataOutputStream(s.getOutputStream());
    Thread sendMessage = new Thread(() -> {
      while (true) {
        String message = input.nextLine();
        try {
          dos.writeUTF(message);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
    Thread readMessage = new Thread(() -> {
      while (true) {
        try {
          String message = dis.readUTF();
          System.out.println(message);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
    sendMessage.start();
    readMessage.start();
  }
}
