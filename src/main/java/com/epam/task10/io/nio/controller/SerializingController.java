package com.epam.task10.io.nio.controller;

import com.epam.task10.io.nio.model.droids.ship.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerializingController extends Executor {

  private DroidsShip deathStar;

  public SerializingController() {
    deathStar = new DroidsShip();
    deathStar.getUnits().add(new Droid("C3PO", 100, 50, 40));
    deathStar.getUnits().add(new DroidMechanic("R2D2", 80, 30, 60));
  }

  private void serializeDroidsShip() throws IOException {
    try (ObjectOutputStream out = new ObjectOutputStream(
        new FileOutputStream(properties.getProperty("droidsShip")))) {
      out.writeObject(deathStar);
    }
  }

  private DroidsShip getDeserializedDroidsShip() throws IOException, ClassNotFoundException {
    DroidsShip deserializedDeadStar;
    try (ObjectInputStream in = new ObjectInputStream(
        new FileInputStream(properties.getProperty("droidsShip")))) {
      deserializedDeadStar = (DroidsShip) in.readObject();
    }
    return deserializedDeadStar;
  }

  @Override
  public List<String> execute() {
    results.clear();
    results.add("Serialized Ship of Droids:");
    for (Attachable unit : deathStar.getUnits()
        ) {
      results.add(unit.toString());
    }
    try {
      serializeDroidsShip();
      results.add("Deserialized Ship of Droids:");
      for (Attachable unit : getDeserializedDroidsShip().getUnits()
          ) {
        results.add(unit.toString());
      }
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    return results;
  }
}
