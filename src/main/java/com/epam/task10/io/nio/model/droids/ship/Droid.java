package com.epam.task10.io.nio.model.droids.ship;

public class Droid extends Unit {

  public Droid(String name, int xp, int power, int defence) {
    super(name, xp, power, defence);
  }

  public String attack() {
    return String.format("%s attack someone", getName());
  }
}
