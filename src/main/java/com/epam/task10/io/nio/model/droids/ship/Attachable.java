package com.epam.task10.io.nio.model.droids.ship;

import java.io.Serializable;

public interface Attachable extends Serializable{

  String attack();
}
