package com.epam.task10.io.nio.controller;

import java.util.List;

public interface Executable {

  List<String> execute();
}
