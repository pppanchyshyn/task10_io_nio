package com.epam.task10.io.nio.model.droids.ship;

public abstract class Unit implements Attachable {

  private String name;
  private int xp;
  private int power;
  private transient int defence;

  Unit(String name, int xp, int power, int defence) {
    this.name = name;
    this.xp = xp;
    this.power = power;
    this.defence = defence;
  }

  String getName() {
    return name;
  }

  @Override
  public String toString() {
    return String.format("%s: xp - %d, power - %d, defense - %d", name, xp, power, defence);
  }

  public abstract String attack();
}
