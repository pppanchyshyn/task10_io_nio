package com.epam.task10.io.nio.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DirectoryContentDisplayController extends Executor {

  public DirectoryContentDisplayController() {
  }

  private Path getPathToDirectory(String command) {
    if (!Paths.get(command).toFile().exists()) {
      logger.warn("Invalid path. Root directory will be displayed");
      return Paths.get(properties.getProperty("root"));
    } else {
      return Paths.get(command);
    }
  }

  private List<String> getDirectoryObjects(Path path) {
    return Arrays.asList(Objects.requireNonNull(path.toFile().list()));
  }

  private String getCurrentDirectoryName(Path path) {
    return path.toFile().getName();
  }

  @Override
  public List<String> execute() {
    results.clear();
    logger.info("Enter path to some directory: ");
    Path path = getPathToDirectory(consoleReader.enterPath());
    results.add(String.format("%s : ", getCurrentDirectoryName(path)));
    results.addAll(getDirectoryObjects(path));
    return results;
  }
}
