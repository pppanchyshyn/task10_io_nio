package com.epam.task10.io.nio.controller;

import com.epam.task10.io.nio.utilities.ConsoleReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class Executor implements Executable {
  Logger logger = LogManager.getLogger(Executor.class);
  Properties properties;
  List<String> results;
  ConsoleReader consoleReader;

  Executor() {
    properties = new Properties();
    results = new ArrayList<>();
    consoleReader = new ConsoleReader();
    try {
      properties.load(new FileInputStream("src/main/resources/Path.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
