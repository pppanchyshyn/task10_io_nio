package com.epam.task10.io.nio.utilities;

public class Constants {

  public final static int EXIT_MENU_ITEM = 5;
  public final static int[] BUFFER_SIZE = {
      (int) (0.1 * 1024 * 1024),
      (int) (0.2 * 1024 * 1024),
      (int) (0.3 * 1024 * 1024),
      (int) (0.5 * 1024 * 1024),
      1024 * 1024,
      2 * 1024 * 1024
  };
  public final static int MEGA_COEFFICIENT = 1024 * 1024;
  public final static String MAIN_MENU = "To print serialization demo press 0\n"
      + "To print results of comparing reading and writing performance of usual and buffered "
      + "readers press 1\n"
      + "To display content of a specific directory  press 2\n"
      + "To display the comments of Java source-code file press 3\n"
      + "To print results of work custom input stream press 4\n"
      + "To test messenger stop this program and start main method of Server, Client1 and Client2\n"
      + "To exit program press 5\n"
      + "\nMake your choice: ";
  public final static String INPUT_STREAM_TEST_STRING = "h--llo p--opl----)";
  public final static int INPUT_STREAM_BUFFER_SIZE = 2;

  private Constants() {
  }
}
