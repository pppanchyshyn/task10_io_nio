package com.epam.task10.io.nio.model.custom.input.stream;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomInputStream extends FilterInputStream {

  private byte[] buffer;
  private int position;

  public CustomInputStream(InputStream inputStream, int bufferSize) {
    super(inputStream);
    if (bufferSize <= 0) {
      throw new IllegalArgumentException("Invalid size! must be > 0");
    }
    buffer = new byte[bufferSize];
    position = bufferSize;
  }

  public int read() throws IOException {
    return super.read();
  }

  public void unread(int b) throws IOException {
    if (position == 0) {
      throw new IOException("Buffer is full");
    }
    buffer[--position] = (byte) b;
  }

  public void close() throws IOException {
    if (in == null) {
      return;
    }
    in.close();
    in = null;
    buffer = null;
  }
}