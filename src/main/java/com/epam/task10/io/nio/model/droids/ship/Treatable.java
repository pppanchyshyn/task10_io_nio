package com.epam.task10.io.nio.model.droids.ship;

public interface Treatable {

  String treat();
}
