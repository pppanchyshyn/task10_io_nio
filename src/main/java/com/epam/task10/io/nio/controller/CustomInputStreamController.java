package com.epam.task10.io.nio.controller;

import com.epam.task10.io.nio.model.custom.input.stream.CustomInputStream;
import com.epam.task10.io.nio.utilities.Constants;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

public class CustomInputStreamController extends Executor {

  private String testString;

  public CustomInputStreamController() {
    testString = Constants.INPUT_STREAM_TEST_STRING;
  }

  private String customStreamDemo(){
    byte byteArray[] = testString.getBytes();
    StringBuilder stringBuilder = new StringBuilder();
    try (CustomInputStream in = new CustomInputStream(new ByteArrayInputStream(byteArray),
        Constants.INPUT_STREAM_BUFFER_SIZE)) {
      int character;
      while ((character = in.read()) != -1) {
        if (character == '-') {
          int nextCharacter;
          if ((nextCharacter = in.read()) == '-') {
            stringBuilder.append("e");
          } else {
            in.unread(nextCharacter);
            stringBuilder.append((char) character);
          }
        } else {
          stringBuilder.append((char) character);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return stringBuilder.toString();
  }

  @Override
  public List<String> execute() {
    results.clear();
    results.add(String.format("String before reading %s", Constants.INPUT_STREAM_TEST_STRING));
    results.add(String.format("String after reading %s", customStreamDemo()));
    return results;
  }
}
