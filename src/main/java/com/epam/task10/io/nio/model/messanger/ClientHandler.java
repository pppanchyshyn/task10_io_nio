package com.epam.task10.io.nio.model.messanger;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

public class ClientHandler implements Runnable {

  private String name;
  private DataInputStream dis;
  private DataOutputStream dos;
  private Socket socket;
  private boolean isLogged;

  ClientHandler(Socket s, String name, DataInputStream dis, DataOutputStream dos) {
    this.dis = dis;
    this.dos = dos;
    this.name = name;
    this.socket = s;
    this.isLogged = true;
  }

  @Override
  public void run() {
    String received;
    while (true) {
      try {
        received = dis.readUTF();
        System.out.println(received);
        if (received.equals("logout")) {
          this.isLogged = false;
          this.socket.close();
          break;
        }
        StringTokenizer st = new StringTokenizer(received, "@");
        String MsgToSend = st.nextToken();
        String recipient = st.nextToken();
        for (ClientHandler mc : Server.vector) {
          if (mc.name.equals(recipient) && mc.isLogged) {
            mc.dos.writeUTF(this.name + " : " + MsgToSend);
            break;
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try {
      this.dis.close();
      this.dos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
