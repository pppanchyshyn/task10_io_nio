package com.epam.task10.io.nio.view;

import com.epam.task10.io.nio.controller.*;
import com.epam.task10.io.nio.utilities.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements Printable{

  private Logger logger = LogManager.getLogger(ConsoleView.class);
  private Map<Integer, Executable> mainMenu;
  private ConsoleReader consoleReader;

  ConsoleView() {
    consoleReader = new ConsoleReader();
    mainMenu = new HashMap<>();
    mainMenu.put(0, new SerializingController());
    mainMenu.put(1, new ReadingAndWritingFileController());
    mainMenu.put(2, new DirectoryContentDisplayController());
    mainMenu.put(3, new CommentDisplayController());
    mainMenu.put(4, new CustomInputStreamController());
  }

  private void printList(List<String> collection) {
    collection.forEach(s->logger.info(s));
  }

  @Override
  public void print() {
    int menuItem;
    while (true) {
      logger.info(Constants.MAIN_MENU);
      menuItem = consoleReader.enterMenuItem();
      if (menuItem == Constants.EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      logger.info("-------------------------------------------------------------------------");
      printList(mainMenu.get(menuItem).execute());
      logger.info("-------------------------------------------------------------------------");
    }
  }
}
