package com.epam.task10.io.nio.model.droids.ship;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DroidsShip implements Serializable{

  private List<Attachable> units;

  public DroidsShip() {
    units = new ArrayList<>();
  }

  public List<Attachable> getUnits() {
    return units;
  }
}
