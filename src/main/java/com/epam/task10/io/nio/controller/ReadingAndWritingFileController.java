package com.epam.task10.io.nio.controller;

import com.epam.task10.io.nio.utilities.Constants;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class ReadingAndWritingFileController extends Executor {

  private long startTime;
  private long endTime;

  public ReadingAndWritingFileController() {
  }

  private long getReadingTimeWithoutBuffer() throws IOException {

    try (FileReader in =
        new FileReader(properties.getProperty("book2mb"))) {
      int data = in.read();
      startTime = System.currentTimeMillis();
      while (data != -1) {
        data = in.read();
      }
      endTime = System.currentTimeMillis();
    }
    return endTime - startTime;
  }

  private long getReadingTimeWithBuffer(int bufferSize) throws IOException {
    try (
        BufferedReader in = new BufferedReader(
            new FileReader(properties.getProperty("book2mb")), bufferSize)) {
      int data = in.read();
      startTime = System.currentTimeMillis();
      while (data != -1) {
        data = in.read();
      }
      endTime = System.currentTimeMillis();
    }
    return endTime - startTime;
  }

  private void writeStrings(Writer writer) throws IOException {
    for (int i = 0; i < 100000; i++) {
      writer.write(String.format("String %d\n", i));
    }
  }

  private long getWritingTimeWithoutBuffer() throws IOException {
    try (FileWriter out = new FileWriter(properties.getProperty("writtenFile"))) {
      startTime = System.currentTimeMillis();
      writeStrings(out);
      endTime = System.currentTimeMillis();
    }
    return endTime - startTime;
  }

  private long getWritingTimeWithBuffer(int bufferSize) throws IOException {
    try (BufferedWriter out = new BufferedWriter(
        new FileWriter(properties.getProperty("writtenFile")), bufferSize)) {
      startTime = System.currentTimeMillis();
      writeStrings(out);
      endTime = System.currentTimeMillis();
    }
    return endTime - startTime;
  }

  private double roundDouble(double number) {
    return new BigDecimal(number).setScale(1, RoundingMode.UP).doubleValue();
  }

  @Override
  public List<String> execute() {
    results.clear();
    try {
      results.add(String.format("Reading of 2 mB file without buffer takes %d milliseconds",
          getReadingTimeWithoutBuffer()));
      for (int bufferSize : Constants.BUFFER_SIZE
          ) {
        results.add(
            String.format("Reading of 2 mB file with buffer %-2.1f mB takes %d milliseconds",
                roundDouble((double) bufferSize / Constants.MEGA_COEFFICIENT),
                getReadingTimeWithBuffer(bufferSize)));
      }
      results
          .add(String
              .format("Writing 100000 chars to file without buffer takes %d milliseconds",
                  getWritingTimeWithoutBuffer()));
      for (int bufferSize : Constants.BUFFER_SIZE
          ) {
        results
            .add(String
                .format(
                    "Writing 100000 chars to file with buffer %-2.1f mB takes %d milliseconds",
                    roundDouble((double) bufferSize / Constants.MEGA_COEFFICIENT),
                    getWritingTimeWithBuffer(bufferSize)));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return results;
  }
}