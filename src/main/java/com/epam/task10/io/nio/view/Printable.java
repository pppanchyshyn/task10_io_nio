package com.epam.task10.io.nio.view;

import java.io.IOException;

public interface Printable {

  void print() throws IOException;
}
