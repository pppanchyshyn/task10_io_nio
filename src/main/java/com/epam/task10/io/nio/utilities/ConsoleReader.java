package com.epam.task10.io.nio.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleReader {

  private Logger logger = LogManager.getLogger(ConsoleReader.class);
  private Scanner input = new Scanner(System.in);

  public ConsoleReader() {
  }

  public int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        logger.info("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  public int enterMenuItem() {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem <= Constants.EXIT_MENU_ITEM)) {
        break;
      } else {
        logger.info("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }
  public String enterPath(){
    return input.nextLine();
  }
}
