package com.epam.task10.io.nio.model.messanger;

import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Server {

  private static Logger logger = LogManager.getLogger(Server.class);
  static Vector<ClientHandler> vector = new Vector<>();
  private static int clientsNumber = 1;

  public static void main(String[] args) throws IOException {
    ServerSocket serverSocket = new ServerSocket(1111);
    Socket socket;
    while (true) {
      socket = serverSocket.accept();
      logger.info("New client request received : " + socket);
      DataInputStream dis = new DataInputStream(socket.getInputStream());
      DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
      logger.info("Creating a new handler for this client...");
      ClientHandler clientHandler = new ClientHandler(socket, "client " + clientsNumber, dis, dos);
      Thread thread = new Thread(clientHandler);
      logger.info("Adding this client to active client list");
      vector.add(clientHandler);
      thread.start();
      clientsNumber++;
    }
  }
}
