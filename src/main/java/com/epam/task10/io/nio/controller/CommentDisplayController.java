package com.epam.task10.io.nio.controller;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CommentDisplayController extends Executor {

  public CommentDisplayController() {
  }

  private List<String> getComments(String path) {
    List<String> comments = new ArrayList<>();
    try (
        DataInputStream in = new DataInputStream(
            new BufferedInputStream(
                new FileInputStream(path)))) {
      while (true) {
        String line = in.readLine();
        if (line == null) {
          break;
        }
        int index = line.indexOf("//");
        if (index != -1) {
          comments.add(line.substring(index));
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return comments;
  }

  private boolean isJavaSourceCode(String name) {
    return name.contains(".java");
  }

  private String getPath(String path) {
    if (Paths.get(path).toFile().exists() && isJavaSourceCode(path)) {
      return path;
    } else {
      throw new IllegalArgumentException("Invalid path");
    }
  }

  @Override
  public List<String> execute() {
    results.clear();
    logger.info("Enter path to some java source-code file: ");
    try {
      String path = getPath(consoleReader.enterPath());
      logger.info("Displaying comments in current file: ");
      results.addAll(getComments(path));
    } catch (IllegalArgumentException e) {
      logger.error(e.getMessage());
    }
    return results;
  }
}