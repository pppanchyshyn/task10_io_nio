package com.epam.task10.io.nio.model.droids.ship;

public class DroidMechanic extends Droid implements Treatable {

  public DroidMechanic(String name, int xp, int power, int defence) {
    super(name, xp, power, defence);
  }

  public String treat() {
    return String.format("%s treat someone", getName());
  }
}
